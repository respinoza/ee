#!/bin/bash

root=${1:-.}

for env in "test" "live"; do
	for org in "org1" "org2"; do
		for space in "space1" "space2"; do
			mkdir -p $root/$env/$org/$space
		done
	done
done

pushd $root
  touch ./live/org1/space2/app1 ./live/org1/space2/app2 ./live/org1/space2/app3
  touch ./live/org2/space1/app1
  touch ./test/org1/space1/app1 ./test/org1/space2/app2
  touch ./test/org2/space1/app1 ./test/org2/space1/app2
popd