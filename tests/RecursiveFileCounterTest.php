<?php

namespace Ee;

use PHPUnit\Framework\TestCase;

class RecursiveFileCounterTest extends TestCase
{
    private const FIXTURE_DIR = '/tmp/phpunit_integration/ee';

    public function testList(): void
    {
        if (!is_dir(self::FIXTURE_DIR)) {
            $this->markTestSkipped(sprintf('No default test structure, use the bash script provided to create [%s]',
                self::FIXTURE_DIR
            ));
        }

        $counter = new RecursiveFileCounter();

        $list = $counter->list(self::FIXTURE_DIR);

        $this->assertEquals(
            [
                self::FIXTURE_DIR.'/test/org2/space2' => 0,
                self::FIXTURE_DIR.'/test/org2/space1' => 2,
                self::FIXTURE_DIR.'/test/org2' => 2,
                self::FIXTURE_DIR.'/test/org1/space2' => 1,
                self::FIXTURE_DIR.'/test/org1/space1' => 1,
                self::FIXTURE_DIR.'/test/org1' => 2,
                self::FIXTURE_DIR.'/test' => 4,
                self::FIXTURE_DIR.'/live/org2/space2' => 0,
                self::FIXTURE_DIR.'/live/org2/space1' => 1,
                self::FIXTURE_DIR.'/live/org2' => 1,
                self::FIXTURE_DIR.'/live/org1/space2' => 3,
                self::FIXTURE_DIR.'/live/org1/space1' => 0,
                self::FIXTURE_DIR.'/live/org1' => 3,
                self::FIXTURE_DIR.'/live' => 4,
            ],
            $list
        );
    }
}
