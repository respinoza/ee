<?php
declare(strict_types=1);

namespace Ee;

class RecursiveFileCounter
{
    public function list(string $path): array
    {
        return $this->traversePostOrder($path);
    }

    private function traversePostOrder(string $path, array &$result = []): array
    {
        $iterator = new \FilesystemIterator($path);
        $fileStack = new \SplStack();

        /**
         * @var string $key
         * @var \SplFileInfo $node
         */
        foreach ($iterator as $key => $node) {
            if (!array_key_exists($key, $result) && $node->isDir()) {
                $result[$key] = 0;
            }

            // process children at the end (post order traversal)
            if ($node->isFile()) {
                $fileStack->push($node);
            }

            if ($node->isDir()) {
                $this->traversePostOrder($node->getPathname(),$result);
            }
        }

        foreach ($fileStack as $file) {
            $result[$file->getPath()]++;
        }

        // discard previous dir count when we are at the root
        $previousDir = \dirname($path, 1);
        if (array_key_exists($previousDir, $result)) {
            $result[$previousDir] += $result[$path];
        }

        return $result;
    }
}