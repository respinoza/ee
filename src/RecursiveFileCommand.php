<?php
declare(strict_types=1);

namespace Ee;

class RecursiveFileCommand
{
    /**
     * @var RecursiveFileCounter
     */
    private $fileCounter;
    /**
     * @var Formatter\RecursiveFileCounter
     */
    private $formatter;

    public function __construct(RecursiveFileCounter $fileCounter, Formatter\RecursiveFileCounter $formatter)
    {
        $this->fileCounter = $fileCounter;
        $this->formatter = $formatter;
    }

    public function run(string $path)
    {
        if (!is_dir($path)) {
            return sprintf("No fixtures found in path %s\n", $path);
        }

        return $this->formatter->format($this->fileCounter->list($path));
    }
}