<?php
declare(strict_types=1);

namespace Ee\Formatter;

class RecursiveFileCounter
{
    public function format(array $dirCount): string
    {
        $result = '';

        foreach ($dirCount as $path => $count) {
            $result .= sprintf("%s contains %d files\n", $path, $count);
        }

        return $result;
    }
}