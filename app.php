#!/usr/bin/env php
<?php

include __DIR__.'/vendor/autoload.php';

$command = new \Ee\RecursiveFileCommand(new \Ee\RecursiveFileCounter(), new \Ee\Formatter\RecursiveFileCounter);

echo $command->run('/tmp/phpunit_integration/ee');