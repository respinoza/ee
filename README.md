How to run
----------

1. Install php (version 7.1+) and typical extensions
2. Run `composer install`
3. Run `composer app` for unit tests

Note: Fixtures will be run in the `/tmp/phpunit_integration/ee` for simplicity (hard coded)


Tests
-----

Just run `composer tests`
